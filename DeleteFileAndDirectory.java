/**
 * Created by Packard bell on 9/17/2015.
 */
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

public class DeleteFileAndDirectory
{
    public static void main(String[] args)
    {
        File file = new File("d\\docs\\input.txt");

        try
        {
            file.delete();
            System.out.println("deleted");
        }
        catch(Exception error)
        {
            System.out.println(error.getMessage());
        }

    }
}
