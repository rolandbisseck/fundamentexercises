import javax.swing.*;

/**
 * Created by Packard bell on 9/17/2015.
 */
public class EvenOrOdd
{
    public static void main(String[] args)
    {
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "enter a number"));
        String result ="";
        if(number % 2 == 0)
        {
            result = number + "is an even number";
        }
        else
        {
            result = number + "is an odd number";
        }
        System.out.println(result);
    }
}
